const services = require('./core/service');
const adscook_db = require('./core/adscook-db');
const adscookMongooseAdapter = require('./core/adscook-mongoose-adapter');
const captureWebsite = require('./core/capture-website');
const base = require('./core/base');

module.exports = {
  services,
  adscook_db,
  adscookMongooseAdapter,
  captureWebsite,
  base
};
