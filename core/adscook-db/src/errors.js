/*
 * moleculer-db
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */


const { MoleculerClientError } = require('moleculer').Errors;

// const ERR_ENTITY_NOT_FOUND = "ERR_ENTITY_NOT_FOUND";

/**
 * Entity not found
 *
 * @class EntityNotFoundError
 * @extends {MoleculerClientError}
 */
class EntityNotFoundError extends MoleculerClientError {
  constructor(id) {
    super('Entity not found', 404, null, {
      id,
    });
  }
}


module.exports = {
  EntityNotFoundError,

  // ERR_ENTITY_NOT_FOUND,
};
