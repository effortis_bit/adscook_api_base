/* eslint-disable no-mixed-operators */
/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
/* eslint-disable consistent-return */
/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/*
 * moleculer-db
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */


const _ = require('lodash');
const Promise = require('bluebird');
const { MoleculerClientError, ValidationError } = require('moleculer').Errors;
const { EntityNotFoundError } = require('./errors');
const MemoryAdapter = require('./memory-adapter');

/**
 * Service mixin to access database entities
 *
 * @name moleculer-db
 * @module Service
 */
module.exports = {
  // Must overwrite it
  name: '',

  // Store adapter (NeDB adapter is the default)
  adapter: null,

  settings: {
    /** @type {String} Name of ID field. */
    idField: '_id',

    /** @type {Array<String>?} Field filtering list. It must be an `Array`. If the value is `null` or `undefined` doesn't filter the fields of entities. */
    fields: null,

    /** @type {Array?} Schema for population. [Read more](#populating). */
    populates: null,

    /** @type {Number} Default page size in `list` action. */
    pageSize: 10,

    /** @type {Number} Maximum page size in `list` action. */
    maxPageSize: 100,

    /** @type {Number} Maximum value of limit in `find` action. Default: `-1` (no limit) */
    maxLimit: -1,

    /** @type {Object|Function} Validator schema or a function to validate the incoming entity in `create` & 'insert' actions. */
    entityValidator: null,
  },


  actions: {
    find: {
      cache: {
        keys: ['populate', 'fields', 'limit', 'offset', 'sort', 'search', 'searchFields', 'query', 'collation'],
      },
      params: {
        populate: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        fields: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        limit: {
          type: 'number', integer: true, min: 0, optional: true, convert: true,
        },
        offset: {
          type: 'number', integer: true, min: 0, optional: true, convert: true,
        },
        sort: { type: 'string', optional: true },
        search: { type: 'string', optional: true },
        searchFields: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        query: { type: 'object', optional: true },
        options: { type: 'object', optional: true },
        collation: { type: 'object', optional: true },
      },
      handler(ctx) {
        const params = this.sanitizeParams(ctx, ctx.params);
        return this._find(ctx, params);
      },
    },

    count: {
      cache: {
        keys: ['search', 'searchFields', 'query'],
      },
      params: {
        search: { type: 'string', optional: true },
        searchFields: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        query: { type: 'object', optional: true },
      },
      handler(ctx) {
        const params = this.sanitizeParams(ctx, ctx.params);
        return this._count(ctx, params);
      },
    },

    list: {
      cache: {
        keys: ['populate', 'fields', 'page', 'pageSize', 'sort', 'search', 'searchFields', 'query'],
      },
      rest: 'GET /',
      params: {
        populate: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        fields: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        page: {
          type: 'number', integer: true, min: 1, optional: true, convert: true,
        },
        pageSize: {
          type: 'number', integer: true, min: 0, optional: true, convert: true,
        },
        sort: { type: 'string', optional: true },
        search: { type: 'string', optional: true },
        searchFields: [
          { type: 'string', optional: true },
          { type: 'array', optional: true, items: 'string' },
        ],
        query: { type: 'object', optional: true },
      },
      handler(ctx) {
        const params = this.sanitizeParams(ctx, ctx.params);
        return this._list(ctx, params);
      },
    },

    create: {
      rest: 'POST /',
      handler(ctx) {
        const { params } = ctx;
        return this._create(ctx, params);
      },
    },

    insert: {
      handler(ctx) {
        const params = this.sanitizeParams(ctx, ctx.params);
        return this._insert(ctx, params);
      },
    },

    get: {
      handler(ctx) {
        return this._get(ctx);
      },
    },

    update: {
      rest: 'PUT /:id',
      handler(ctx) {
        const { params } = ctx;
        const { query } = params;
        return this._update(ctx, params, query);
      },
    },

    updateMany: {
      handler(ctx) {
        const { params } = ctx;
        const { query } = params;
        return this._updateMany(ctx, params, query);
      },
    },

    remove: {
      handler(ctx) {
        return this._remove(ctx);
      },
    },

    removeMany: {
      handler(ctx) {
        return this._removeMany(ctx);
      }
    }, 

    join: {
      handler(ctx) {
        return this._join(ctx);
      },
    },

    aggregate: {
      handler(ctx) {
        return this._aggregate(ctx);
      }
    }

  },

  methods: {

    connect() {
      return this.adapter.connect().then(() => {
        // Call an 'afterConnected' handler in schema
        if (_.isFunction(this.schema.afterConnected)) {
          try {
            return this.schema.afterConnected.call(this);
          } catch (err) {
            /* istanbul ignore next */
            this.logger.error('afterConnected error!', err);
          }
        }
      });
    },

    disconnect() {
      if (_.isFunction(this.adapter.disconnect)) { return this.adapter.disconnect(); }
    },

    sanitizeParams(ctx, params) {
      const p = Object.assign({}, params);

      // Convert from string to number
      if (typeof (p.limit) === 'string') { p.limit = Number(p.limit); }
      if (typeof (p.offset) === 'string') { p.offset = Number(p.offset); }
      if (typeof (p.page) === 'string') { p.page = Number(p.page); }
      if (typeof (p.pageSize) === 'string') { p.pageSize = Number(p.pageSize); }

      if (typeof (p.sort) === 'string') { p.sort = p.sort.replace(/,/g, ' ').split(' '); }

      if (typeof (p.fields) === 'string') { p.fields = p.fields.replace(/,/g, ' ').split(' '); }

      if (typeof (p.populate) === 'string') { p.populate = p.populate.replace(/,/g, ' ').split(' '); }

      if (typeof (p.searchFields) === 'string') { p.searchFields = p.searchFields.replace(/,/g, ' ').split(' '); }

      if (ctx.action.name.endsWith('.list')) {
        // Default `pageSize`
        if (!p.pageSize) { p.pageSize = this.settings.pageSize; }

        // Default `page`
        if (!p.page) { p.page = 1; }

        // Limit the `pageSize`
        if (this.settings.maxPageSize > 0 && p.pageSize > this.settings.maxPageSize) { p.pageSize = this.settings.maxPageSize; }

        // Calculate the limit & offset from page & pageSize
        p.limit = p.pageSize;
        p.offset = (p.page - 1) * p.pageSize;
      }
      // Limit the `limit`
      if (this.settings.maxLimit > 0 && p.limit > this.settings.maxLimit) { p.limit = this.settings.maxLimit; }

      return p;
    },

    entityChanged(type, json, ctx) {
      return this.clearCache().then(() => {
        const eventName = `entity${_.capitalize(type)}`;
        if (this.schema[eventName] != null) {
          return this.schema[eventName].call(this, json, ctx);
        }
      });
    },

    clearCache() {
      this.broker.broadcast(`cache.clean.${this.fullName}`);
      if (this.broker.cacher) { return this.broker.cacher.clean(`${this.fullName}.*`); }
      return Promise.resolve();
    },

    transformDocuments(ctx, params, docs) {
      let isDoc = false;
      if (!Array.isArray(docs)) {
        if (_.isObject(docs)) {
          isDoc = true;
          docs = [docs];
        } else { return Promise.resolve(docs); }
      }

      return Promise.resolve(docs)

      // Convert entity to JS object
        .then(docs => docs.map(doc => this.adapter.entityToObject(doc)))

      // Encode IDs
        .then(docs => docs.map((doc) => {
          doc[this.settings.idField] = this.encodeID(doc[this.settings.idField]);
          return doc;
        }))
      // Apply idField
        .then(docs => docs.map(doc => this.adapter.afterRetrieveTransformID(doc, this.settings.idField)))
      // Populate
        .then(json => ((ctx && params.populate) ? this.populateDocs(ctx, json, params.populate) : json))

      // TODO onTransformHook

      // Filter fields
        .then((json) => {
          let fields = ctx && params.fields ? params.fields : this.settings.fields;

          // Compatibility with < 0.4
          /* istanbul ignore next */
          if (_.isString(fields)) { fields = fields.split(' '); }

          // Authorize the requested fields
          const authFields = this.authorizeFields(fields);

          return json.map(item => this.filterFields(item, authFields));
        })

      // Return
        .then(json => (isDoc ? json[0] : json));
    },

    filterFields(doc, fields) {
      // Apply field filter (support nested paths)
      if (Array.isArray(fields)) {
        const res = {};
        fields.forEach((n) => {
          const v = _.get(doc, n);
          if (v !== undefined) { _.set(res, n, v); }
        });
        return res;
      }

      return doc;
    },

    authorizeFields(fields) {
      if (this.settings.fields && this.settings.fields.length > 0) {
        let res = [];
        if (Array.isArray(fields) && fields.length > 0) {
          fields.forEach((f) => {
            if (this.settings.fields.indexOf(f) !== -1) {
              res.push(f);
              return;
            }

            if (f.indexOf('.') !== -1) {
              const parts = f.split('.');
              while (parts.length > 1) {
                parts.pop();
                if (this.settings.fields.indexOf(parts.join('.')) !== -1) {
                  res.push(f);
                  break;
                }
              }
            }

            const nestedFields = this.settings.fields.filter(prop => prop.indexOf(`${f}.`) !== -1);
            if (nestedFields.length > 0) {
              res = res.concat(nestedFields);
            }
          });
          // return _.intersection(f, this.settings.fields);
        }
        return res;
      }

      return fields;
    },

    populateDocs(ctx, docs, populateFields) {
      if (!this.settings.populates || !Array.isArray(populateFields) || populateFields.length === 0) { return Promise.resolve(docs); }

      if (docs == null || !_.isObject(docs) && !Array.isArray(docs)) { return Promise.resolve(docs); }

      const promises = [];
      _.forIn(this.settings.populates, (rule, field) => {
        if (populateFields.indexOf(field) === -1) { return; } // skip

        // if the rule is a function, save as a custom handler
        if (_.isFunction(rule)) {
          rule = {
            handler: Promise.method(rule),
          };
        }

        // If string, convert to object
        if (_.isString(rule)) {
          rule = {
            action: rule,
          };
        }
        rule.field = field;

        const arr = Array.isArray(docs) ? docs : [docs];

        // Collect IDs from field of docs (flatten, compact & unique list)
        const idList = _.uniq(_.flattenDeep(_.compact(arr.map(doc => doc[field]))));
        // Replace the received models according to IDs in the original docs
        const resultTransform = (populatedDocs) => {
          arr.forEach((doc) => {
            const id = doc[field];
            if (_.isArray(id)) {
              const models = _.compact(id.map(id => populatedDocs[id]));
              doc[field] = models;
            } else {
              doc[field] = populatedDocs[id];
            }
          });
        };

        if (rule.handler) {
          promises.push(rule.handler.call(this, idList, arr, rule, ctx));
        } else if (idList.length > 0) {
          // Call the target action & collect the promises
          const params = Object.assign({
            id: idList,
            mapping: true,
            populate: rule.populate,
          }, rule.params || {});

          promises.push(ctx.call(rule.action, params).then(resultTransform));
        }
      });

      return Promise.all(promises).then(() => docs);
    },

    validateEntity(entity) {
      if (!_.isFunction(this.settings.entityValidator)) { return Promise.resolve(entity); }

      const entities = Array.isArray(entity) ? entity : [entity];
      return Promise.all(entities.map(entity => this.settings.entityValidator.call(this, entity))).then(() => entity);
    },

    encodeID(id) {
      return id;
    },

    decodeID(id) {
      return id;
    },

    _find(ctx, params) {
      return this.adapter.find(params)
      .then(docs => this.transformDocuments(ctx, params, docs));
    },

    _count(ctx, params) {
      // Remove pagination params
      if (params && params.limit) { params.limit = null; }
      if (params && params.offset) { params.offset = null; }
      return this.adapter.count(params);
    },

    _aggregate(ctx){
      const { params } = ctx;
      return this.adapter.aggregate(params);
    },

    _list(ctx, params) {
      const countParams = Object.assign({}, params);
      // Remove pagination params
      if (countParams && countParams.limit) { countParams.limit = null; }
      if (countParams && countParams.offset) { countParams.offset = null; }
      return Promise.all([
        // Get rows
        this.adapter.find(params),
        // Get count of all rows
        this.adapter.count(countParams),
      ]).then(res => this.transformDocuments(ctx, params, res[0])
        .then(docs => ({
          // Rows
          rows: docs,
          // Total rows
          total: res[1],
          // Page
          page: params.page,
          // Page size
          pageSize: params.pageSize,
          // Total pages
          totalPages: Math.floor((res[1] + params.pageSize - 1) / params.pageSize),
        })));
    },

    _create(ctx, params) {
      const entity = params;
      return this.validateEntity(entity)
      // Apply idField
        .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
        .then(entity => this.adapter.insert(entity))
        .then(doc => this.transformDocuments(ctx, {}, doc))
        .then(json => this.entityChanged('created', json, ctx).then(() => json));
    },

    _insert(ctx, params) {
      return Promise.resolve()
        .then(() => {
          if (Array.isArray(params.entities)) {
            return this.validateEntity(params.entities)
            // Apply idField
              .then((entities) => {
                if (this.settings.idField === '_id') { return entities; }
                return entities.map(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField));
              })
              .then(entities => this.adapter.insertMany(entities));
          } else if (params.entity) {
            return this.validateEntity(params.entity)
            // Apply idField
              .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
              .then(entity => this.adapter.insert(entity));
          }
          return Promise.reject(new MoleculerClientError("Invalid request! The 'params' must contain 'entity' or 'entities'!", 400));
        })
        .then(docs => this.transformDocuments(ctx, params, docs))
        .then(json => this.entityChanged('created', json, ctx).then(() => json));
    },

    _get(ctx) {
      // let origDoc;
      const query = ctx.params;
      const { _id } = query;

      return Promise.resolve()
        .then(() => this.adapter.findOne(_id, query)).then(doc => this.transformDocuments(ctx, ctx.params, doc));
    },

    _update(ctx, params, query) {
      try {
        const sets = {};
        delete params.query;
        let { options } = params;
        options = options || {};
        delete params.options;
        // Convert fields from params to "$set" update object
        Object.keys(params).forEach((prop) => {
          sets[prop] = params[prop];
        });
        return this.adapter.updateById(query, { $set: sets }, options)
          .then((doc) => {
            if (!doc) { return Promise.reject(new EntityNotFoundError(query._id)); }
            return this.transformDocuments(ctx, params, doc)
              .then(json => this.entityChanged('updated', json, ctx).then(() => json));
          });
      } catch (err) {
        throw err;
      }
    },

    _updateMany(ctx, params, query){
      try {
        const sets = {};
        delete params.query;
        let { options } = params;
        options = options || {};
        delete params.options;
        // Convert fields from params to "$set" update object
        Object.keys(params).forEach((prop) => {
          sets[prop] = params[prop];
        });
        return this.adapter.updateMany(query, { $set: sets }, options)
          .then((doc) => {
            if (!doc) { return Promise.reject(new EntityNotFoundError(query._id)); }
            return this.transformDocuments(ctx, params, doc)
              .then(json => this.entityChanged('updated', json, ctx).then(() => json));
          });
      } catch (err) {
        throw err;
      }
    },

    _remove(ctx) {
      try {
        return Promise.resolve()
          .then(() => this.adapter.removeById(ctx.params).then((data) => {
            if (data) return data;
            return Promise.reject(new EntityNotFoundError());
          }));
      } catch (err) {
        throw err;
      }
    },

    _removeMany(ctx) {
      try {
        return Promise.resolve()
          .then(() => this.adapter.removeMany(ctx.params).then((data) => {
            if (data) return data;
            return Promise.reject(new EntityNotFoundError());
          }));
      } catch (err) {
        throw err;
      }
    },

    _join(ctx) {
      try {
        return Promise.resolve()
          .then(() => this.adapter.join(ctx.params).then((data) => {
            console.log(data);
            if (data) return data;
            return Promise.reject(new EntityNotFoundError());
          }));
      } catch (err) {
        throw err;
      }
    },
  },

  /**
* Service created lifecycle event handler
*/
  created() {
    // Compatibility with < 0.4
    if (_.isString(this.settings.fields)) {
      this.settings.fields = this.settings.fields.split(' ');
    }

    if (!this.schema.adapter) { this.adapter = new MemoryAdapter(); } else { this.adapter = this.schema.adapter; }

    this.adapter.init(this.broker, this);

    // Transform entity validation schema to checker function
    if (this.broker.validator && _.isObject(this.settings.entityValidator) && !_.isFunction(this.settings.entityValidator)) {
      const check = this.broker.validator.compile(this.settings.entityValidator);
      this.settings.entityValidator = (entity) => {
        const res = check(entity);
        if (res === true) { return Promise.resolve(); }
        return Promise.reject(new ValidationError('Entity validation error!', null, res));
      };
    }
  },

  started() {
    if (this.adapter) {
      return new Promise((resolve) => {
        const connecting = () => {
          this.connect().then(resolve).catch((err) => {
            this.logger.error('Connection error!', err);
            setTimeout(() => {
              this.logger.warn('Reconnecting...');
              connecting();
            }, 1000);
          });
        };

        connecting();
      });
    }

    /* istanbul ignore next */
    return Promise.reject(new Error('Please set the store adapter in schema!'));
  },


  stopped() {
    if (this.adapter) { return this.disconnect(); }
  },

  // Export Memory Adapter class
  MemoryAdapter,
};
