const baseConfig = {
    limit: 10,
    offset: 0,
    sortBy: '-created_at',
  };
  
  module.exports = { baseConfig };
  
  