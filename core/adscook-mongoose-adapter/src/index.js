/* eslint-disable no-param-reassign */
/* eslint-disable no-lonely-if */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-underscore-dangle */
/* eslint-disable new-cap */
/* eslint-disable no-sequences */
/* eslint-disable no-unused-expressions */
/*
 * moleculer-db-adapter-mongoose
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */


const _ = require('lodash');
const Promise = require('bluebird');
const { ServiceSchemaError } = require('moleculer').Errors;
const mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

class MongooseDbAdapter {
  constructor(uri, opts) {
    this.uri = uri,
    this.opts = opts;
    mongoose.Promise = Promise;
  }

  init(broker, service) {
    this.broker = broker;
    this.service = service;

    if (this.service.schema.model) {
      this.model = this.service.schema.model;
    } else if (this.service.schema.schema) {
      if (!this.service.schema.modelName) {
        throw new ServiceSchemaError('`modelName` is required when `schema` is given in schema of service!');
      }
      this.schema = this.service.schema.schema;
      this.modelName = this.service.schema.modelName;
    }

    if (!this.model && !this.schema) {
      /* istanbul ignore next */
      throw new ServiceSchemaError('Missing `model` or `schema` definition in schema of service!');
    }
  }

  connect() {
    let conn;

    if (this.model) {
      /* istanbul ignore next */
      if (mongoose.connection.readyState === 1) {
        this.db = mongoose.connection;
        return Promise.resolve();
      } else if (mongoose.connection.readyState === 2) {
        conn = mongoose.connection;
      } else {
        conn = mongoose.connect(this.uri, this.opts);
      }
    } else if (this.schema) {
      conn = mongoose.createConnection(this.uri, this.opts);
      this.model = conn.model(this.modelName, this.schema);
    }

    return conn.then((result) => {
      this.conn = conn;

      if (result.connection) { this.db = result.connection.db; } else { this.db = result.db; }

      this.service.logger.info('MongoDB adapter has connected successfully.');

      /* istanbul ignore next */
      this.db.on('disconnected', () => this.service.logger.warn('Mongoose adapter has disconnected.'));
      this.db.on('error', err => this.service.logger.error('MongoDB error.', err));
      this.db.on('reconnect', () => this.service.logger.info('Mongoose adapter has reconnected.'));
    });
  }

  disconnect() {
    return new Promise((resolve) => {
      if (this.db && this.db.close) {
        this.db.close(resolve);
      } else {
        mongoose.connection.close(resolve);
      }
    });
  }

  find(filters) {
    const data = this.createCursor(filters).exec();
    return data;
  }

  findOne(_id, query) {
    const condition = {
      _id,
    };
    if (query.user_id) condition.user_id = query.user_id;
    return this.model.findOne(condition).exec();
  }

  findById(_id) {
    return this.model.findById(_id).exec();
  }

  findByIds(idList, query) {
    try {
      const condition = {
        _id: {
          $in: idList,
        },
      };
      if (query.user_id) condition.user_id = query.user_id;
      return this.model.find(condition).exec();
    } catch (err) {
      throw err;
    }
  }

  count(filters = {}) {
    return this.createCursor(filters).countDocuments().exec();
  }

  insert(entity) {
    const item = new this.model(entity);
    return item.save();
  }

  insertMany(entities) {
    return this.model.create(entities);
  }

  updateMany(query, update, options) {
    options.multi = true;
    options.new = true;
    return this.model.updateMany(query, update, options).then(res => res.n);
  }

  updateById(query, update, options) {
    options.new = true;
    return this.model.findOneAndUpdate(query, update, options);
  }

  aggregate(query){
    return this.model.aggregate(query);
  }

  removeMany(query) {
    return this.model.deleteMany(query);
  }

  removeById(query) {
    try {
      const condition = {
        _id: query._id,
      };
      if (query.user_id) condition.user_id = query.user_id;
      return this.model.findOneAndRemove(condition);
    } catch (err) {
      throw err;
    }
  }

  join(params) {
    try {
      const { lookup, match } = params;
      const data = this.model.aggregate([{
        $match: match,
      }, {
        $lookup: lookup,
      }]);
      return data;
    } catch (err) {
      throw err;
    }
  }

  clear() {
    return this.model.deleteMany({}).then(res => res.n);
  }


  entityToObject(entity) {
    const json = entity.toJSON();
    if (entity._id && entity._id.toHexString) {
      json._id = entity._id.toHexString();
    } else if (entity._id && entity._id.toString) {
      json._id = entity._id.toString();
    }
    return json;
  }

  createCursor(params) {
    if (params) {

      const options = params.options || {};
      const q = this.model.find(params.query, options);

      // Full-text search
      // More info: https://docs.mongodb.com/manual/reference/operator/query/text/
      if (_.isString(params.search) && params.search !== '') {
        q.find({
          $text: {
            $search: params.search,
          },
        });
        q._fields = {
          _score: {
            $meta: 'textScore',
          },
        };
        q.sort({
          _score: {
            $meta: 'textScore',
          },
        });
      } else {
        // Sort
        if (_.isString(params.sort)) { q.sort(params.sort.replace(/,/, ' ')); } else if (Array.isArray(params.sort)) { q.sort(params.sort.join(' ')); }
      }

      // Offset
      if (_.isNumber(params.offset) && params.offset > 0) { q.skip(params.offset); }

      // Limit
      if (_.isNumber(params.limit) && params.limit > 0) { q.limit(params.limit); }

      return q;
    }
    return this.model.find();
  }

  beforeSaveTransformID(entity, idField) {
    const newEntity = _.cloneDeep(entity);

    if (idField !== '_id' && entity[idField] !== undefined) {
      newEntity._id = this.stringToObjectID(newEntity[idField]);
      delete newEntity[idField];
    }

    return newEntity;
  }

  afterRetrieveTransformID(entity, idField) {
    if (idField !== '_id') {
      entity[idField] = this.objectIDToString(entity._id);
      delete entity._id;
    }
    return entity;
  }

  stringToObjectID(id) {
    if (typeof id === 'string' && mongoose.Types.ObjectId.isValid(id)) { return new mongoose.Schema.Types.ObjectId(id); }
    return id;
  }

  objectIDToString(id) {
    if (id && id.toString) { return id.toString(); }
    return id;
  }
}

module.exports = MongooseDbAdapter;