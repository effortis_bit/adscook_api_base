/* eslint-disable camelcase,no-underscore-dangle */
const { Service, Errors: { MoleculerError } } = require('moleculer');

class BaseAdscookService extends Service {
  /**
   * @constructor
   * @param {*} broker - moleculer Broker instance
   */
  constructor(broker) {
    super(broker);
    this.models = {};
  }

  // lifecycle events
  serviceCreated() {
    this.logger.info(`${this.name} Service created.`);
  }
  serviceStarted() {
    this.logger.info(`${this.name} Service started.`);
  }
  serviceStopped() {
    this.logger.info(`${this.name} Service stopped.`);
  }

  /**
 *
 * @param {string} msg
 * @param {number} code
 * @param {string} type
 * @param {object} data
 */
  raiseError(msg = '', code = 500, type = 'code level exception', data = {}) {
    this.logger.info(msg, code, type, data);
    throw new MoleculerError(msg, code, type, data);
  }
  /**
   * global exceptions handler
   * @param {*} err - exception object
   * @param {number} status - status code
   * @param {string} funcName - parent function string identifier
   */
  async handleError(err, status, funcName) {
    this.logger.info(err.message, err, funcName);
    if (err instanceof MoleculerError) {
      throw err;
    }
    throw new MoleculerError(err.message, status || err.code || 500, funcName || 'exception', err);
  }
}

module.exports = BaseAdscookService;
