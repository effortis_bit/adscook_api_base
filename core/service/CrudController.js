/* eslint-disable camelcase,no-underscore-dangle */
const { baseConfig } = require('../config/config');
const { MoleculerError } = require('moleculer').Errors;

class CrudController {
  constructor(config) {
    this.config = config;
    this.baseConfig = baseConfig;
  }

  async getById(ctx) {
    try {
      const { id, fields } = ctx.params;
      const { broker } = ctx;
      const query = {
        _id: id,
        fields,
      };
      if (this.config.getWithUserId) query.user_id = ctx.meta.user.id;
      const data = await broker.call(`${this.config.db}.get`, query);

      return {
        success: 1,
        data,
      };
    } catch (err) {
      if (err.code === 404) return new MoleculerError('Entity not found', 404, 'MONGO_ERR', err);
      return new MoleculerError('Something went wrong during get rule by id', 500, 'MONGO_ERR', err);
    }
  }

  async get(ctx) {
    try {
      const { broker } = ctx;
      const { limit, offset, sortBy, fields } = ctx.meta;
      const db_params = {
        limit: limit || this.config.limit || this.baseConfig.limit,
        offset: offset || this.config.offset || this.baseConfig.offset,
        sort: sortBy || this.config.sortBy || this.baseConfig.sortBy,
        fields,
      };
      if (this.config.getWithUserId) db_params.query = { user_id: ctx.meta.user.id }
      const data = await broker.call(`${this.config.db}.find`, db_params);
      return {
        success: 1,
        data,
      };
    } catch (err) {
      if (err.code === 404) return new MoleculerError('Entity not found', 404, 'MONGO_ERR', err);
      return new MoleculerError('Something went wrong during get rules list', 500, 'MONGO_ERR', err);
    }
  }

  async delete(ctx) {
    try {
      const query = {
        _id: ctx.params.id,
      };
      const { broker } = ctx;
      if (this.config.deleteWithUserId) query.user_id = ctx.meta.user.id;
      await broker.call(`${this.config.db}.remove`, query);
      return {
        success: 1,
      };
    } catch (err) {
      if (err.code === 404) return new MoleculerError('Entity not found', 404, 'Entity not found');
      return new MoleculerError('Something went wrong during delete rule', 500, 'MONGO_ERR', err);
    }
  }

  async create(ctx) {
    try {
      const { createParams } = this.config;
      const { broker } = ctx;
      const db_params = {};
      if (this.config.createWithUserId) db_params.user_id = ctx.meta.user.id;
      createParams.forEach((el) => {
        if (ctx.params[el]) db_params[el] = ctx.params[el];
      });
      const response = await broker.call(`${this.config.db}.create`, db_params);

      return {
        success: 1,
        data: response,
      };
    } catch (err) {
      console.log(err, 'eeeeer');
      return new MoleculerError('Something went wrong during create rule', 500, 'MONGO_ERR', err);
    }
  }

  async update(ctx) {
    try {
      const { createParams } = this.config;
      const { broker } = ctx;
      const db_params = {};
      createParams.forEach((el) => {
        if (ctx.params[el]) db_params[el] = ctx.params[el];
      });
      db_params.id = ctx.params.id;
      if (this.config.updateWithUserId) db_params.query.user_id = ctx.meta.user.id;
      db_params.query = {
        _id: ctx.params.id,
      };

      await broker.call(`${this.config.db}.update`, db_params);
      return {
        success: 1,
      };
    } catch (err) {
      if (err.code === 404) return new MoleculerError('Entity not found', 404, 'MONGO_ERR', err);
      return new MoleculerError('Something went wrong during update rule', 500, 'MONGO_ERR', err);
    }
  }
}

module.exports = CrudController;
