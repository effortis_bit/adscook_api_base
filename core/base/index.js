module.exports.contextStringify = (ctx) => {

  return JSON.stringify(ctx,  function(key, value) {
      if( key == 'req'){
        return {
          url: value.url,
          method: value.method,
          body: value.body,
          query: value.query,
          parsedUrl: value.parsedUrl,
          originalUrl: value.originalUrl,
          aborted: value.aborted,
          headers: value.headers,
          rawHeaders: value.rawHeaders
        };
      } else if( key == 'res' ){
        return;
      }
      return value;
  })

}
